<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 8/2/17
 * Time: 5:36 PM
 */

namespace AppBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class MainController extends Controller
{
    public function homepageAction()
    {
        return $this->render('main/homepage.html.twig');
    }
}